<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">

	<!-- CSS only -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

	<!-- JS, Popper.js, and jQuery -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


	<!-- Ajax cdn-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</head>
<body>
	<div id="showdata">
		<form class="reg-form" action="process.php" method="post" enctype="multipart/form-data">
			<h2 class="text-center">Student Registration</h2><br>

			<div class="field-row">
				<label class="form-label" for="firstName">First name</label>
				<input type="text" id="firstName" class="field text-field first-name-field" name="fname" >
				<h6 id="fnamechack"></h6>
				<h6 id="fnamechackv"></h6>

			</div>

			<div class="field-row">
				<label class="form-label cf" for="lastName">Last name</label>
				<input type="text" id="lastName" class="field text-field last-name-field"  name="lname">
				<h6 id="lnamechack"></h6>
				<h6 id="lnamechackv"></h6>


			</div>

			<div class="field-row">
				<label class="form-label" for="initials">Father Name</label>
				<input type="text" id="initials" class="field text-field initials-field"v name="faname">
				<h6 id="fanamechack"></h6>
				<h6 id="fanamechackv"></h6>

			</div>

			<div class="field-row">
				<label class="form-label" for="dateOfBirth">Date of birth</label>
				<input type="date" id="dateOfBirth" class="field date-field dob-field" min="1900-01-01" max="2020-09-18" name="udob"  >
				<h6 id="dobchack"></h6>

			</div>

			<div class="field-row">
				<label class="form-label" for="tel">Telephone</label>
				<input type="tel" id="tel" class="field text-field tel-field" name="phone">
				<h6 id="tnochack"></h6>

			</div>


			<div class="field-row">
				<label class="form-label" for="email">Email</label>
				<input type="email" id="email" class="field text-field email-field" name="uemail" >
				<h6 id="emailchack"></h6>

			</div>

			<div class="form-check" >
				<input type="radio" name="exampleRadios2" id="science" value="science">
				<label>
					SCIENCE
				</label>
			</div>

			<div class="form-check" id="sciencelist">
				<input type="checkbox" name="sub1" id="physics" value="physics" >
				<label for="subject1">Physics</label><br>
				<input type="checkbox" name="sub2" id="chemestry" value="chemestry">
				<label for="subject2">Chemstry</label><br>
				<input type="checkbox" name="sub3" id="mathmatices" value="mathmatices">
				<label for="subject3">Mathematics</label>
			</div>



			<div class="form-check">
				<input type="radio" name="exampleRadios2" id="commerce" value="commerce">
				<label>
					COMMERCE
				</label>
			</div>

			<div class="form-check" id="commercelist">
				<input type="checkbox" name="sub4" id="history" value="history">
				<label for="subject1">History</label><br>
				<input type="checkbox" name="sub5" id="ecommerce" value="ecommerce">
				<label for="subject2">Ecomerce</label><br>
				<input type="checkbox" name="sub6" id="accounting" value="accounting">
				<label for="subject3">Accounting</label><br><br>
			</div>
			<div id="checkcheck"></div>


			<input type='file' id="imgInp" name="uimage"><br>
			<img id="blah" src="#" alt="your image">
			<div id="imagechack">
			</div>




			<div>
				<button type=submit class="btn btn-primary">Register</button>
			</div>





		</form>
	</div>
</body>
<script src="main.js"></script>
</html> 